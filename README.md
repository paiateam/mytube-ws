# MyTube REST Web Service #

Java EE REST web service to upload content meta-information. Part of a practice of Distributed Computing course (Computing Engineering at [Universitat de Lleida](http://udl.cat/ca/en/))

## Project dependencies ##

In order to use this project you will need:

* mytube-RPC project (You can found this [here](https://bitbucket.org/paiateam/mytube-rpc))
* Java 8
* Java EE with JBoss server
* Installation of JDBC driver in JBoss
* A relational database (in our case we have used PostgreSQL)

## Web Service REST API ##

### SERVERS ###

**/servers**

* **_POST_**: Register of a server

    * Request: ```{"serverIP": "localhost", "serverPort": 1099}```
    * Response: ```{"serverID":"722de2d3-3e7e-4d6e-acfc-edc6bfcff372", "serverURL": "rmi://localhost:1099/722de2d3-3e7e-4d6e-acfc-edc6bfcff372"}```

* **_GET_**: List of all registered servers
    
    * Response: ```[{"serverID":"33f9fb0b-2985-4373-9d4d-3cae3dfe2d8c","serverURL":"rmi://localhost:1099/33f9fb0b-2985-4373-9d4d-3cae3dfe2d8c","serverAddress":"localhost:1099"}]```

**/servers/{server-id}**

* **_GET_**: Retrieve _server-id_ information

    * Response: ```{"serverID":"33f9fb0b-2985-4373-9d4d-3cae3dfe2d8c","serverURL":"rmi://localhost:1099/33f9fb0b-2985-4373-9d4d-3cae3dfe2d8c","serverAddress":"localhost:1099"}```

### USERS ###

**/users**

* **_POST_**: Register of a user

    * Request: ```{"userName": "user1", "password": "1234", "serverID": "33f9fb0b-2985-4373-9d4d-3cae3dfe2d8c"}```
    * Response: ```{"userID": "a83075c4-4c5d-4b28-9022-80b72a0a7bc7"}```

**/users/{user-id}**

* **_GET_**: Retrieve _user-id_ information

    * Response: ```{"userName": "user1", "userID": "a83075c4-4c5d-4b28-9022-80b72a0a7bc7"}```

### CONTENTS ###

**/contents**

* **_POST_**: Upload a content

    * Request: ```{"serverID":"<serverID>", "userID":"<userID>", "title": "<title>", "description": "<description>"}```
    * Response: ```{"contentID":"<contentID>"}```
    * For conflict: _status 409_ and ```{“msg”:<content ID of conflict>}```

**/contents/{content-id}**

* **_GET_**: Retrieve content-id information

    * Response: ```{"contentID":"<contentID>", "serverID":"<serverID>", "userID":"<userID>", "title": "<title>", "description": "<description>"}```

**/contents/{content-id}?userID={user-id}**

* **_DELETE_**: Deletes content if it belongs to the user.
* **_PUT_**: Modifies content if it belongs to the user

    * Request: title, description or both (e.g. ```{"title": "new title"}```
    * Response: same as download

### For errors ###
* Response: ```{“msg”:”message”}```
