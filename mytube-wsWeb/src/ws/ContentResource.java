package ws;

import java.util.List;
import java.util.UUID;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.ContentBean;
import dao.ContentDAO;
import dao.ContentDAODataBase;
import dao.DataAccessException;
import dao.ServerDAO;
import dao.ServerDAODataBase;
import dao.UserDAO;
import dao.UserDAODataBase;
import data.Content;
import utilities.ErrorResponses;


@RequestScoped
@Path("/contents")
public class ContentResource {
	
	// ----------------------
	// ------ CONTENTS ------
	// ----------------------
	
	private ServerDAO serverDAO;
	private ContentDAO contentDAO;
	private UserDAO userDAO;
	
	public ContentResource() {
		try {
			this.serverDAO = ServerDAODataBase.getInstance();
			this.contentDAO = ContentDAODataBase.getInstance();
			this.userDAO = UserDAODataBase.getInstance();
		} catch (DataAccessException e) {
			System.out.println("[DB ERROR] " + e.getMessage());
			System.exit(-1);
		}
	}
	
	// ====== Upload Content ======
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response uploadContent(ContentBean contentBean) {
		try {
			if (contentBean.hasAllAttributes()) {
				// All arguments are passed correctly.
				UUID serverUUID;
				try {
					serverUUID = UUID.fromString(contentBean.getServerID());
				} catch(IllegalArgumentException e) {
					// Unknown server
					return ErrorResponses.forbidden("Unknown server ID: " + contentBean.getServerID()); 
				}
				if (serverDAO.getServer(serverUUID) != null) {
					// Good request and known server
					UUID userUUID;
					try {
						userUUID = UUID.fromString(contentBean.getUserID());
					} catch(IllegalArgumentException e) {
						// Unknown user
						return ErrorResponses.forbidden("Unknown user ID: " + contentBean.getUserID());
					}
					if (userDAO.retreiveUser(userUUID) != null) {
						// Good request and known user, content can be processed
						return uploadContentGoodRequest(contentBean.getTitle(),
														contentBean.getDescription(),
														serverUUID, userUUID);
					} else {
						// Unknown user
						return ErrorResponses.forbidden("Unknown user ID: " + contentBean.getUserID());
					}
				} else {
					// Unknown server
					return ErrorResponses.forbidden("Unknown server ID: " + contentBean.getServerID());
				}
			}
			// Bad request, missing arguments
			return ErrorResponses.badRequest("Missing attributes in content JSON");
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	private Response uploadContentGoodRequest(String title, String description, UUID serverUUID, UUID userUUID) {
		try {
			Content storedContent = contentDAO.getUserContent(title, userUUID);
			if (storedContent == null) {
				// Content of user "userUUID" with title "title" not created. Can store the content.
				Content content = new Content(title, description, userUUID, serverUUID);
				contentDAO.saveContent(content);
				System.out.println("[CONTENT CREATED] Content with title " + title + " created by user " + userUUID);
				String json = "{\"contentID\":\"" + content.getContentID() + "\"}";
				return Response.status(201).entity(json).build();
			}
			// User had already created this content with the same title. Can not store.
			return previouslyUploadedContent(title, storedContent.getContentID());
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	private Response previouslyUploadedContent(String title, UUID contentID) {
		System.out.println("[CONTENT ERROR] The user already has a content with the title " + title);
		String json = "{\"msg\":\"" + contentID.toString() + "\"}";
		return Response.status(409).entity(json).build();
	}
	
	// ====== Download Content ======
	@Path("/{contentID}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getContent(@PathParam("contentID") String contentID) {
		// Check contentID param
		if (contentID.equals("") || contentID == null) {
			return ErrorResponses.badRequest("contentID parameter is mandatory");
		}
		try {
			UUID contentUUID;
			try {
				contentUUID = UUID.fromString(contentID);
			} catch(IllegalArgumentException e) {
				// Content not found
				return ErrorResponses.notFound("Content with ID " + contentID + " not found");
			}
			Content content = contentDAO.getContent(contentUUID);
			if (content == null) {
				// content not found
				return ErrorResponses.notFound("Content with ID " + contentID + " not found");
			}
			// Content found
			return getContentGoodRequest(content);
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	private Response getContentGoodRequest(Content content) {
		System.out.println("[OK] Content " + content.getContentID() + " dispatched");
		String json = "{\"contentID\":\"" + content.getContentID() + "\"," +
						"\"userID\":\"" + content.getUserID() + "\"," + 
						"\"serverID\":\"" + content.getServerID() + "\"," +
						"\"title\":\"" + content.getTitle() + "\"," +
						"\"description\":\"" + content.getDescription() + "\"}";
		return Response.status(200).entity(json).build();
	}
	
	// ====== Delete Content ======
	@Path("/{contentID}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteContent(@PathParam("contentID") String contentID,
									@QueryParam("userID") String userID) {
		try {
			if (userID == null || userID.equals("")) {
				// userID query param not provided
				return ErrorResponses.badRequest("userID parameter is mandatory");
			}
			UUID contentUUID;
			try {
				contentUUID = UUID.fromString(contentID);
			} catch(IllegalArgumentException e) {
				// Content not found
				return ErrorResponses.notFound("Content with ID " + contentID + " not found");
			}
			UUID userUUID;
			try {
				userUUID = UUID.fromString(userID);
			} catch(IllegalArgumentException e) {
				// User not found
				return ErrorResponses.notFound("User with ID " + userID + " not found");
			}
			if (!contentDAO.doesContentExist(contentUUID)) {
				// content not found
				return ErrorResponses.notFound("Content with ID " + contentID + " not found");
			}
			// Content found
			return deleteContentGoodRequest(contentUUID, userUUID);
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	private Response deleteContentGoodRequest(UUID contentID, UUID userID) {
		try {
			if (contentDAO.isContentOfUser(contentID, userID)) {
				System.out.println("[DELETE] Content " + contentID + " deleted");
				contentDAO.deleteContent(contentID);
				return Response.status(200).build();
			}
			return ErrorResponses.forbidden("The user is not the owner of the resource");
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	// ====== Modify Content ======
	@Path("/{contentID}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response modifyContent(ContentBean contentBean,
								  @PathParam("contentID") String contentID,
								  @QueryParam("userID") String userID) {
		try {
			boolean ok = false;
			// Check userID query param
			if (userID == null || userID.equals("")) {
				// userID query param not provided
				return ErrorResponses.badRequest("userID parameter is mandatory");
			}
			// Ensure that content exists
			UUID contentUUID;
			try {
				contentUUID = UUID.fromString(contentID);
			} catch(IllegalArgumentException e) {
				// Content not found
				return ErrorResponses.notFound("Content with ID " + contentID + " not found");
			}
			if (!contentDAO.doesContentExist(contentUUID)) {
				return ErrorResponses.notFound("Content with ID " + contentID + " not found"); 
			}
			// Get userUUID
			UUID userUUID;
			try {
				userUUID = UUID.fromString(userID);
			} catch(IllegalArgumentException e) {
				// User not found
				return ErrorResponses.notFound("User with ID " + userID + " not found");
			}
			// Ensure that user is the owner of the content
			if (!contentDAO.isContentOfUser(contentUUID, userUUID)) {
				return ErrorResponses.forbidden("The user is not the owner of the resource"); 
			}
			// Content Bean has title to be modified
			if (contentBean.hasTitle()) {
				Content storedContent = contentDAO.getUserContent(contentBean.getTitle(), userUUID);
				if (storedContent == null) {
					// no conflicts with new title
					contentDAO.modifyTitle(contentUUID, contentBean.getTitle());
					System.out.println("[MODIFIED] Title of content " + contentID +
										" modified to " + contentBean.getTitle());
					ok = true;
				} else {
					System.out.println("[CONFLICT] New title of content " + contentID +
										"conflicts with content " + storedContent.getContentID().toString());
					return previouslyUploadedContent(contentBean.getTitle(), contentUUID);
				}
			}
			// Content Bean has description to be modified
			if (contentBean.hasDescription()) {
				contentDAO.modifyDescription(contentUUID, contentBean.getDescription());
				System.out.println("[MODIFIED] Description of content " + contentID + " modified to " + contentBean.getDescription());
				ok = true;
			}
			// Bad request, title or description not provided | Good request, return modified content
			return ok == false ? ErrorResponses.badRequest("Title, description or both must be provided")
							   : getContentGoodRequest(contentDAO.getContent(contentUUID));
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	// ====== Search Contents ======
	@Path("/search")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchContent(
			@DefaultValue("") @QueryParam("userID") String userID,
			@DefaultValue("") @QueryParam("terms") String terms) {
		// userID and terms provided
		if (!userID.equals("") && !terms.equals("")) {
			return searchContentByUserAndTerms(userID, terms);
		}
		// userID provided
		if (!userID.equals("")) {
			return searchContentByUser(userID);
		}
		// terms provided
		if (!terms.equals("")) {
			return searchContentByTerms(terms);
		}
		// No information to search
		return ErrorResponses.badRequest("userID or terms must be provided to search contents");
	}
	
	private Response searchContentByUserAndTerms(String userID, String terms) {
		try {
			// Get userUUID
			UUID userUUID;
			try {
				userUUID = UUID.fromString(userID);
			} catch(IllegalArgumentException e) {
				// User not found
				return ErrorResponses.notFound("User with ID " + userID + " not found");
			}
			List<String> contentsID = contentDAO.getContentsOfUserByTerms(userUUID, terms);
			if (contentsID == null) {
				// no contents
				return ErrorResponses.notFound("Requested contents not found");
			}
			return searchContentGoodRequest(contentsID);
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	private Response searchContentByUser(String userID) {
		try {
			// Get userUUID
			UUID userUUID;
			try {
				userUUID = UUID.fromString(userID);
			} catch(IllegalArgumentException e) {
				// User not found
				return ErrorResponses.notFound("User with ID " + userID + " not found");
			}
			List<String> contentsID = contentDAO.getContentsOfUser(userUUID);
			if (contentsID == null) {
				// no contents
				return ErrorResponses.notFound("Requested contents not found");
			}
			return searchContentGoodRequest(contentsID);
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	private Response searchContentByTerms(String terms) {
		try {
			List<String> contentsID = contentDAO.getContentsByTerms(terms);
			if (contentsID == null) {
				// no contents
				return ErrorResponses.notFound("Requested contents not found");
			}
			return searchContentGoodRequest(contentsID);
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	private Response searchContentGoodRequest(List<String> contentsID) {
		System.out.println("[OK] Search dispatched successfully");
		StringBuilder json = new StringBuilder("{\"contents\":["); // start of json
		for (String id: contentsID) {
			json.append("{\"contentID\":\"" + id + "\"},"); // append all contentIDS
		}
		json.deleteCharAt(json.length() - 1); // remove last comma
		json.append("]}"); // append end of json
		return Response.status(200).entity(json.toString()).build();
	}
}
