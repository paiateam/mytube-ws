package ws;

import java.util.UUID;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.ServerBean;
import dao.DataAccessException;
import dao.ServerDAO;
import dao.ServerDAODataBase;
//import dao.ServerDAOSimpleImpl;
import data.Server;
import utilities.ErrorResponses;

@RequestScoped
@Path("/servers")
public class ServerResource {
	
	// ---------------------
	// ------ SERVERS ------
	// ---------------------

	private ServerDAO serverDAO;
	
	public ServerResource() {
		try {
			this.serverDAO = ServerDAODataBase.getInstance();
		} catch (DataAccessException e) {
			System.out.println("[DB ERROR] " + e.getMessage());
			System.exit(-1);
		}
		
	}
	
	// ====== Register Server ======
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerServer(ServerBean serverBean) {
		if (serverBean.hasAllAttributes()) {
			return registerServerGoodRequest(serverBean);
		}
		return ErrorResponses.badRequest("Missing argument in server register/login");
	}
	
	private Response registerServerGoodRequest(ServerBean serverBean) {
		try {
			Server storedServer = serverDAO.getServer(serverBean.getServerIP() + ":" + serverBean.getServerPort());
			if (storedServer == null) {
				// Creation of server
				Server server = new Server(serverBean.getServerIP(), serverBean.getServerPort());
				serverDAO.registryServer(server);
				System.out.println("[SERVER CREATED] Server in " + server.getServerAddress() +
									" created with ID " + server.getServerID());
				String json = "{\"serverID\":\"" + server.getServerID() + "\", \"serverURL\":\"" + server.getServerURL() + "\"}";
				return Response.status(201).entity(json).build();
			}
			// Retrieve previous server URL
			System.out.println("[SERVER LOG IN] Server in " + storedServer.getServerAddress() + " logged in");
			String json = "{\"serverID\":\"" + storedServer.getServerID() + "\", \"serverURL\":\"" + storedServer.getServerURL() + "\"}";
			return Response.status(200).entity(json).build();
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	// ====== List Servers ======
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Server[] getServerList() {
		try {
			return serverDAO.getServersList();
		} catch (DataAccessException e) {
			return new Server[0];
		}
	}
	
	//====== Get Server URL ======
	@GET
	@Path("/{server-id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getServerURL(@PathParam("server-id") String serverID){
		try {
			UUID serverUUID;
			try {
				serverUUID = UUID.fromString(serverID);
			} catch (IllegalArgumentException e) {
				// Unknown server ID
				return ErrorResponses.badRequest("Unknown server ID format");
			}
			Server server = serverDAO.getServer(serverUUID);
			if (server == null) {
				// User not found
				return ErrorResponses.notFound("Server with ID " + serverUUID + " not found");
			}
			// User found
			return getServerRequest(server);
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
		
	private Response getServerRequest(Server server) {
		System.out.println("[OK] Requested Server with ID " + server.getServerID());
		String json = "{\"serverID\":\"" + server.getServerID() + "\", \"serverURL\":\"" + server.getServerURL() + "\"}";
		return Response.status(200).entity(json).build();
	}
}
