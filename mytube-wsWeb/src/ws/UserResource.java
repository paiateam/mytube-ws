package ws;

import java.util.UUID;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.UserBean;
import dao.DataAccessException;
import dao.ServerDAO;
import dao.ServerDAODataBase;
import dao.UserDAO;
import dao.UserDAODataBase;
import data.User;
import utilities.ErrorResponses;

@RequestScoped
@Path("/users")
public class UserResource {
	
	// -------------------
	// ------ USERS ------
	// -------------------
	
	private UserDAO userDAO;
	private ServerDAO serverDAO;
	
	public UserResource() {
		try {
			this.userDAO = UserDAODataBase.getInstance();
			this.serverDAO = ServerDAODataBase.getInstance();
		} catch (DataAccessException e) {
			System.out.println("[DB ERROR] " + e.getMessage());
			System.exit(-1);
		}
	}
	
	// ====== Registry User ======
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerUser(UserBean userBean) {
		try {
			if (userBean.hasAllAttributes()) {
				UUID serverUUID;
				try {
					serverUUID = UUID.fromString(userBean.getServerID());
				} catch(IllegalArgumentException e) {
					// Unknown server
					return ErrorResponses.forbidden("Unknown server ID: " + userBean.getServerID()); 
				}
				if (serverDAO.getServer(serverUUID) != null) {
					// Good request and known server
					return registerUserGoodRequest(userBean);
				} else {
					// Unknown server
					return ErrorResponses.forbidden("Unknown server ID: " + userBean.getServerID()); 
				}
			}
			// Bad request
			return ErrorResponses.badRequest("Missing argument in user register/login");
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	private Response registerUserGoodRequest(UserBean userBean) {
		try {
			String json;
			User storedUser = userDAO.retreiveUser(userBean.getUserName());
			if (storedUser == null) {
				// User creation
				User user = new User(userBean.getUserName(), userBean.getPassword());
				userDAO.saveUser(user);
				System.out.println("[USER CREATED] User " + user.getUserName() + " created with ID " + user.getUserID());
				json = "{\"userID\":\"" + user.getUserID() + "\"}";
				return Response.status(201).entity(json).build();
			} else if (storedUser.getPassword().equals(userBean.getPassword())) {
				// User retrieve
				System.out.println("[USER LOG IN] User " + storedUser.getUserName() + " logged in");
				json = "{\"userID\":\"" + storedUser.getUserID() + "\"}";
				return Response.status(200).entity(json).build();
			} else {
				// Wrong password for this user
				return ErrorResponses.forbidden("User " + userBean.getUserName() + " has provided an incorrect password");
			}
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	// ====== Get User ======
	@Path("/{user-id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("user-id") String userID) {
		try {
			UUID userUUID;
			try {
				userUUID = UUID.fromString(userID);
			} catch (IllegalArgumentException e) {
				// Unknown user ID
				return ErrorResponses.badRequest("Unknown user ID format");
			}
			User user = userDAO.retreiveUser(userUUID);
			if (user == null) {
				// User not found
				return ErrorResponses.notFound("User with ID " + userUUID + " not found");
			}
			// User found
			return getUserGoodRequest(user);
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
	
	private Response getUserGoodRequest(User user) {
		System.out.println("[OK] Requested user " + user.getUserName() + " with ID " + user.getUserID());
		String json = "{\"userName\":\"" + user.getUserName() + "\", \"userID\":\"" + user.getUserID() + "\"}";
		return Response.status(200).entity(json).build();
	}
	
	// ====== Get User ======
	@Path("/search")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchUser(@DefaultValue("") @QueryParam("userName") String userName) {
		try {
			if (userName.equals("")) {
				// empty user name
				return ErrorResponses.badRequest("userName must be provided");
			}
			User user = userDAO.retreiveUser(userName);
			if (user == null) {
				// user not found
				return ErrorResponses.notFound("User " + userName + " not found");
			}
			// OK
			System.out.println("[OK] User " + userName + " dispatched");
			String json = "{\"userID\":\"" + user.getUserID() + "\"}";
			return Response.status(200).entity(json).build();
		} catch (DataAccessException e) {
			return ErrorResponses.internalError(e.getMessage());
		}
	}
}

