package data;

import java.util.UUID;

public class Server {
	
	private final UUID serverID;
	private final String serverIP;
	private final int serverPort;
	private final String serverURL;
	
	public Server(String serverIP, int serverPort) {
		this.serverIP = serverIP;
		this.serverPort = serverPort;
		this.serverID = UUID.randomUUID();
		this.serverURL = "rmi://" + this.serverIP + ":" + this.serverPort + "/" + this.serverID;
	}
	
	public Server(String serverID, String serverIP, int serverPort, String serverURL) {
		this.serverID = UUID.fromString(serverID);
		this.serverIP = serverIP;
		this.serverPort = serverPort;
		this.serverURL = serverURL;
	}
	
	public int getServerPort() {
		return this.serverPort;
	}
	
	public String getServerIP() {
		return this.serverIP;
	}
	
	public String getServerURL() {
		return this.serverURL;
	}
	
	public UUID getServerID() {
		return this.serverID;
	}
	
	public String getServerAddress() {
		return this.serverIP + ":" + this.serverPort;
	}
}
