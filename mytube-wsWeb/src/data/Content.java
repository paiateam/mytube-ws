package data;

import java.util.UUID;

public class Content {
	private final UUID contentID;
	private String title;
	private String description;
	private final UUID userID;
	private final UUID serverID;
	
	public Content(String title, String description, UUID userID, UUID serverID) {
		this.contentID = UUID.randomUUID();
		this.title = title;
		this.userID = userID;
		this.description = description;
		this.serverID = serverID;
	}
	
	public Content(String contentID, String title, String description, String userID, String serverID) {
		this.contentID = UUID.fromString(contentID);
		this.title = title;
		this.userID = UUID.fromString(userID);
		this.description = description;
		this.serverID = UUID.fromString(serverID);
	}
	
	public UUID getContentID() {
		return this.contentID;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public UUID getUserID() {
		return this.userID;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public UUID getServerID() {
		return this.serverID;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}
