package data;

import java.util.UUID;

public class User {
	private final UUID userID;
	private final String userName;
	private final String password;
	
	
	public User(String userName, String password) {
		this.userID = UUID.randomUUID();  // Creation of user ID
		this.userName = userName;
		this.password = password;
	}
	
	public User(String userID, String userName, String password) {
		this.userID = UUID.fromString(userID);
		this.userName = userName;
		this.password = password;
	}
	
	public UUID getUserID() {
		return this.userID;
	}
	
	public String getUserName() {
		return this.userName;
	}

	public String getPassword() {
		return password;
	}
}
