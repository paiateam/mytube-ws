package beans;

public final class ServerBean {
	private String serverIP;
	private int serverPort;
	
	public ServerBean() { }
	
	public ServerBean(String serverIP, int serverPort) {
		this.serverIP = serverIP;
		this.serverPort = serverPort;
	}
	
	public String getServerIP() {
		return this.serverIP;
	}
	
	public int getServerPort() {
		return this.serverPort;
	}
	
	public boolean hasAllAttributes() {
		return !(this.serverIP == null) && !(this.serverPort == 0);
	}
}
