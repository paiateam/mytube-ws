package beans;

public final class UserBean {
	private String userName;
	private String password;
	private String serverID;
	
	public UserBean() {
		this.userName = "";
		this.password = "";
		this.serverID = "";
	}
	
	public UserBean(String userName, String password, String serverID) {
		this.userName = userName;
		this.password = password;
		this.serverID = serverID;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getServerID() {
		return serverID;
	}
	
	public boolean hasAllAttributes() {
		return !(this.userName == null) &&
			   !(this.password == null) &&
			   !(this.serverID == null);
	}
	
	public boolean hasAllAttributesExceptPassword() {
		return !(this.userName == null) &&
			   (this.password == null) &&
			   !(this.serverID == null);
	}
}
