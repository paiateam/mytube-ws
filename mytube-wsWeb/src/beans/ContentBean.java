package beans;

public final class ContentBean {
	private String title;
	private String userID;
	private String description;
	private String serverID;
	
	public ContentBean() { }
	
	public ContentBean(String title, String description, String userID, String serverID) {
		this.title = title;
		this.userID = userID;
		this.description = description;
		this.serverID = serverID;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public String getUserID() {
		return this.userID;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String getServerID() {
		return this.serverID;
	}
	
	public boolean hasAllAttributes() {
		return !(this.title == null) &&
			   !(this.userID == null) &&
			   !(this.description == null) &&
			   !(this.serverID == null);
	}
	
	public boolean hasTitle() {
		return !(this.title == null);
	}
	
	public boolean hasDescription() {
		return !(this.description == null);
	}
}
