package utilities;

import javax.ws.rs.core.Response;

/********************************************
 *   Static class that encapsulates the     *
 * common error responses of the resources  *
 ********************************************/

public final class ErrorResponses {
	
	private ErrorResponses() { }
	
	public static Response badRequest(String msg) {
		System.out.println("[BAD REQUEST] " + msg);
		String json = "{\"msg\":\"" + msg + "\"}";
		return Response.status(400).entity(json).build();
	}
	
	public static Response forbidden(String msg) {
		System.out.println("[FORBIDDEN] " + msg);
		String json = "{\"msg\":\"" + msg + "\"}";
		return Response.status(403).entity(json).build();
	}
	
	public static Response notFound(String msg) {
		System.out.println("[NOT FOUND] " + msg);
		String json = "{\"msg\":\"" + msg + "\"}";
		return Response.status(404).entity(json).build();
	}
	
	public static Response internalError(String msg) {
		System.out.println("[INTERNAL ERROR] " + msg);
		String json = "{\"msg\":\"" + msg + "\"}";
		return Response.status(500).entity(json).build();
	}
	
}
