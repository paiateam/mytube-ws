package dao;

import java.util.UUID;

import data.User;

public interface UserDAO {
	void saveUser(User user) throws DataAccessException;
	User retreiveUser(String username) throws DataAccessException;
	User retreiveUser(UUID userID) throws DataAccessException;
}
