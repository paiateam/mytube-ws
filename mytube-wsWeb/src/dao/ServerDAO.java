package dao;

import java.util.UUID;

import data.Server;

public interface ServerDAO {
	void registryServer(Server server) throws DataAccessException;
	Server getServer(UUID serverID) throws DataAccessException;
	Server getServer(String serverAddress) throws DataAccessException;
	Server[] getServersList() throws DataAccessException;
}
