package dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import data.Server;

public class ServerDAOSimpleImpl implements ServerDAO {
	
	private static ServerDAOSimpleImpl instance;
	
	private Map<UUID, Server> serversMap;
	private Map<String, Server> serversAddressMap;
	
	public static ServerDAOSimpleImpl getInstance() throws DataAccessException {
		if (instance == null) {
			instance = new ServerDAOSimpleImpl();
		}
		return instance;
	}
	
	private ServerDAOSimpleImpl() {
		this.serversMap = new HashMap<>();
		this.serversAddressMap = new HashMap<>();
	}
	
	@Override
	public void registryServer(Server server) throws DataAccessException {
		this.serversMap.put(server.getServerID(), server);
		this.serversAddressMap.put(server.getServerAddress(), server);
	}

	@Override
	public Server getServer(UUID serverID) throws DataAccessException {
		return this.serversMap.get(serverID);
	}
	
	@Override
	public Server getServer(String serverAddress) throws DataAccessException {
		return this.serversAddressMap.get(serverAddress);
	}

	@Override
	public Server[] getServersList() throws DataAccessException {
		Collection<Server> sCollection = this.serversMap.values();
		return sCollection.toArray(new Server[sCollection.size()]);
	}

}
