package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.naming.NamingException;

import data.Content;


public class ContentDAODataBase implements ContentDAO {
	
	private static ContentDAODataBase instance;
	private DataBaseManager dataBase;
	private static final String table = "Contents";
	
	private ContentDAODataBase() throws NamingException, SQLException {
		dataBase = DataBaseManager.getInstance();
	}
	
	public static ContentDAODataBase getInstance() throws DataAccessException {
		if (instance == null) {
			try {
				instance = new ContentDAODataBase();
			} catch (NamingException | SQLException e) {
				throw new DataAccessException("Error connecting to the database");
			}
			
		}
		return instance;
	}
	
	@Override
	public void saveContent(Content content) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("contentID", content.getContentID().toString());
		query.addParam("title", content.getTitle());
		query.addParam("description", content.getDescription());
		query.addParam("serverID", content.getServerID().toString());
		query.addParam("userID", content.getUserID().toString());
		String sql = query.insert();
		System.out.println("[DB QUERY] " + sql);
		try {
			dataBase.insert(sql);
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	@Override
	public Content getContent(UUID contentID) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("contentID", contentID.toString());
		String sql = query.select();
		System.out.println("[DB QUERY] " + sql);
		try {
			ResultSet resultSet = dataBase.query(sql);
			List<Content> contents = retrieveContents(resultSet);
			if (contents.isEmpty()) {
				return null;
			}
			return contents.get(0);
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}
	
	private List<Content> retrieveContents(ResultSet resultSet) throws SQLException {
		List<Content> contents = new LinkedList<>();
		while (resultSet.next()) {
			contents.add(new Content(resultSet.getString("contentID"),
										resultSet.getString("title"),
										resultSet.getString("description"),
										resultSet.getString("userID"),
										resultSet.getString("serverID")));
		}
		return contents;
	}

	@Override
	public Content getUserContent(String title, UUID userID) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("title", title);
		query.addParam("userID", userID.toString());
		String sql = query.select();
		System.out.println("[DB QUERY] " + sql);
		try {
			ResultSet resultSet = dataBase.query(sql);
			List<Content> contents = retrieveContents(resultSet);
			if (contents.isEmpty()) {
				return null;
			}
			return contents.get(0);
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	@Override
	public List<String> getContentsOfUser(UUID userID) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("userID", userID.toString());
		String sql = query.select();
		System.out.println("[DB QUERY] " + sql);
		try {
			ResultSet resultSet = dataBase.query(sql);
			List<Content> contents = retrieveContents(resultSet);
			if (contents.isEmpty()) {
				return null;
			}
			return contentsListToIDsList(contents);
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}
	
	private List<String> contentsListToIDsList(List<Content> contents) {
		return contents.stream()
				.map(x -> x.getContentID().toString())
				.collect(Collectors.toList());
	}

	@Override
	public List<String> getContentsByTerms(String terms) throws DataAccessException {
		String sql = "SELECT * FROM " + table + " WHERE title LIKE '%" + terms +
						"%' OR description LIKE '%" + terms + "%';";
		System.out.println("[DB QUERY] " + sql);
		try {
			ResultSet resultSet = dataBase.query(sql);
			List<Content> contents = retrieveContents(resultSet);
			if (contents.isEmpty()) {
				return null;
			}
			return contentsListToIDsList(contents);
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	@Override
	public List<String> getContentsOfUserByTerms(UUID userID, String terms) throws DataAccessException {
		String sql = "SELECT * FROM " + table + " WHERE (title LIKE '%" + terms +
				"%' OR description LIKE '%" + terms + "%') AND userID='" + userID.toString() + "';";
		System.out.println("[DB QUERY] " + sql);
		try {
			ResultSet resultSet = dataBase.query(sql);
			List<Content> contents = retrieveContents(resultSet);
			if (contents.isEmpty()) {
				return null;
			}
			return contentsListToIDsList(contents);
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	@Override
	public boolean isContentOfUser(UUID contentID, UUID userID) throws DataAccessException {
		Content content = this.getContent(contentID);
		return content.getUserID().equals(userID);
	}

	@Override
	public boolean doesContentExist(UUID contentID) throws DataAccessException {
		return this.getContent(contentID) != null;
	}

	@Override
	public void deleteContent(UUID contentID) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("contentID", contentID.toString());
		String sql = query.delete();
		System.out.println("[DB QUERY] " + sql);
		try {
			dataBase.delete(sql);
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	@Override
	public void modifyTitle(UUID contentID, String title) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("contentID", contentID.toString());
		query.addParam("title", title);
		String[] paramsToSet = {"title"};
		String sql = query.update(paramsToSet);
		System.out.println("[DB QUERY] " + sql);
		try {
			dataBase.update(sql);
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	@Override
	public void modifyDescription(UUID contentID, String description) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("contentID", contentID.toString());
		query.addParam("description", description);
		String[] paramsToSet = {"description"};
		String sql = query.update(paramsToSet);
		System.out.println("[DB QUERY] " + sql);
		try {
			dataBase.update(sql);
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}
}
