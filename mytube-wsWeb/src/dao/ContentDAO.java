package dao;

import java.util.List;
import java.util.UUID;


import data.Content;

public interface ContentDAO {
	void saveContent(Content content) throws DataAccessException;
	Content getContent(UUID contentID) throws DataAccessException;
	Content getUserContent(String title, UUID userID) throws DataAccessException;
	List<String> getContentsOfUser(UUID userID) throws DataAccessException;
	List<String> getContentsByTerms(String terms) throws DataAccessException;
	List<String> getContentsOfUserByTerms(UUID userID, String terms) throws DataAccessException;
	boolean isContentOfUser(UUID contentID, UUID userID) throws DataAccessException;
	boolean doesContentExist(UUID contentID) throws DataAccessException;
	void deleteContent(UUID contentID) throws DataAccessException;
	void modifyTitle(UUID contentID, String title) throws DataAccessException;
	void modifyDescription(UUID contentID, String description) throws DataAccessException;
}
