package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import javax.naming.NamingException;

import data.Server;

public class ServerDAODataBase implements ServerDAO {
	
	private static ServerDAODataBase instance;
	private DataBaseManager dataBase;
	private static final String table = "Servers";
	
	private ServerDAODataBase() throws NamingException, SQLException {
		dataBase = DataBaseManager.getInstance();
	}
	
	public static ServerDAODataBase getInstance() throws DataAccessException {
		if (instance == null) {
			try {
				instance = new ServerDAODataBase();
			} catch (NamingException | SQLException e) {
				throw new DataAccessException("Error connecting to the database");
			}
			
		}
		return instance;
	}
	
	@Override
	public void registryServer(Server server) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("serverID", server.getServerID().toString());
		query.addParam("serverIP", server.getServerIP());
		query.addParam("serverPort", server.getServerPort());
		query.addParam("serverURL", server.getServerURL());
		String sql = query.insert();
		System.out.println("[DB QUERY] " + sql);
		try {
			dataBase.insert(sql);
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	@Override
	public Server getServer(UUID serverID) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("serverID", serverID.toString());
		String sql = query.select();
		System.out.println("[DB QUERY] " + sql);
		try {
			ResultSet resultSet = dataBase.query(sql);
			Server[] servers = retrieveServer(resultSet);
			if (servers.length == 0) {
				return null;
			}
			return servers[0];
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}
	
	private Server[] retrieveServer(ResultSet resultSet) throws SQLException {
		List<Server> servers = new LinkedList<>();
		while (resultSet.next()) {
			servers.add(new Server(resultSet.getString("serverID"),
									resultSet.getString("serverIP"),
									resultSet.getInt("serverPort"),
									resultSet.getString("serverURL")));
		}
		Server[] result = new Server[servers.size()];
		servers.toArray(result);
		return result;
	}

	@Override
	public Server getServer(String serverAddress) throws DataAccessException {
		String[] parts = serverAddress.split(":");
		String serverIP = parts[0];
		int serverPort = Integer.parseInt(parts[1]);
		SQLQuery query = new SQLQuery(table);
		query.addParam("serverIP", serverIP);
		query.addParam("serverPort", serverPort);
		String sql = query.select();
		System.out.println("[DB QUERY] " + sql);
		try {
			ResultSet resultSet = dataBase.query(sql);
			Server[] servers = retrieveServer(resultSet);
			if (servers.length == 0) {
				return null;
			}
			return servers[0];
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	@Override
	public Server[] getServersList() throws DataAccessException {
		String sql = "SELECT * FROM " + table;
		System.out.println("[DB QUERY] " + sql);
		try {
			ResultSet resultSet = dataBase.query(sql);
			Server[] servers = retrieveServer(resultSet);
			return servers;
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}
}
