package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import javax.naming.NamingException;

import data.User;

public class UserDAODataBase implements UserDAO {
	
	private static final String table = "Users";
	
	private static UserDAODataBase instance;
	private DataBaseManager dataBase;
	
	private UserDAODataBase() throws NamingException, SQLException {
		dataBase = DataBaseManager.getInstance();
	}
	
	public static UserDAODataBase getInstance() throws DataAccessException {
		if (instance == null) {
			try {
				instance = new UserDAODataBase();
			} catch (NamingException | SQLException e) {
				throw new DataAccessException("Error connecting to the database");
			}
			
		}
		return instance;
	}

	@Override
	public void saveUser(User user) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("userID", user.getUserID().toString());
		query.addParam("userName", user.getUserName());
		query.addParam("password", user.getPassword());
		String sql = query.insert();
		System.out.println("[DB QUERY] " + sql);
		try {
			dataBase.insert(sql);
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	@Override
	public User retreiveUser(String username) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("userName", username);
		String sql = query.select();
		System.out.println("[DB QUERY] " + sql);
		try {
			ResultSet resultSet = dataBase.query(sql);
			User[] users = retrieveUsers(resultSet);
			if (users.length == 0) {
				return null;
			}
			return users[0];
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}
	
	private User[] retrieveUsers(ResultSet resultSet) throws SQLException {
		List<User> users = new LinkedList<>();
		while (resultSet.next()) {
			users.add(new User(resultSet.getString("userID"),
									resultSet.getString("userName"),
									resultSet.getString("password")));
		}
		User[] result = new User[users.size()];
		users.toArray(result);
		return result;
	}

	@Override
	public User retreiveUser(UUID userID) throws DataAccessException {
		SQLQuery query = new SQLQuery(table);
		query.addParam("userID", userID.toString());
		String sql = query.select();
		System.out.println("[DB QUERY] " + sql);
		try {
			ResultSet resultSet = dataBase.query(sql);
			User[] users = retrieveUsers(resultSet);
			if (users.length == 0) {
				return null;
			}
			return users[0];
		} catch (SQLException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

}
