package dao;

import java.util.LinkedHashMap;
import java.util.Map;

public class SQLQuery {
	
	private String table;
	private Map<String, String> paramsMap;

	public SQLQuery(String table) {
		this.table = table;
		this.paramsMap = new LinkedHashMap<>();
	}
	
	public void addParam(String paramName, String value) {
		this.paramsMap.put(paramName, "'" + value + "'");
	}
	
	public void addParam(String paramName, int value) {
		this.paramsMap.put(paramName, Integer.toString(value));
	}
	
	public String insert() {
		StringBuilder builder = new StringBuilder("INSERT INTO ");
		builder.append(this.table);
		builder.append(buildInsertParams());
		builder.append(";");
		return builder.toString();
	}
	
	private String buildInsertParams() {
		StringBuilder keys = new StringBuilder(" (");
		StringBuilder values = new StringBuilder(" VALUES (");
		for (String key: paramsMap.keySet()) {
			keys.append(key);
			keys.append(",");
			values.append(paramsMap.get(key));
			values.append(",");
		}
		// remove last commas
		keys.deleteCharAt(keys.length() - 1);
		values.deleteCharAt(values.length() - 1);
		// close parenthesis
		keys.append(")");
		values.append(")");
		return keys.toString() + values.toString();
	}
	
	public String delete() {
		StringBuilder builder = new StringBuilder("DELETE FROM ");
		builder.append(this.table);
		builder.append(buildQueryParams());
		builder.append(";");
		return builder.toString();
	}
	
	private String buildQueryParams() {
		StringBuilder params = new StringBuilder(" WHERE ");
		String previousSeparator = "";
		for (String key: paramsMap.keySet()) {
			params.append(previousSeparator);
			params.append(key);
			params.append("=");
			params.append(paramsMap.get(key));
			previousSeparator = " AND ";
		}
		return params.toString();
	}
	
	public String update(String[] paramsToSet) {
		StringBuilder builder = new StringBuilder("UPDATE ");
		builder.append(this.table);
		builder.append(buildSetParams(paramsToSet));
		builder.append(buildQueryParams());
		builder.append(";");
		return builder.toString();
	}
	
	private String buildSetParams(String[] paramsToSet) {
		StringBuilder params = new StringBuilder(" SET ");
		String previousSeparator = "";
		for (String key: paramsToSet) {
			params.append(previousSeparator);
			params.append(key);
			params.append("=");
			params.append(paramsMap.get(key));
			previousSeparator = " AND ";
			paramsMap.remove(key);  // param not needed in where statement
		}
		return params.toString();
	}
	
	public String select() {
		StringBuilder builder = new StringBuilder("SELECT * FROM ");
		builder.append(this.table);
		builder.append(buildQueryParams());
		builder.append(";");
		return builder.toString();
	}
}
