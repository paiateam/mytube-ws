package dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import data.Content;

public class ContentDAOSimpleImpl implements ContentDAO {
	
	private static ContentDAOSimpleImpl instance;
	
	private Map<UUID, Content> contentIDMap;
	private Map<UUID, Map<String, Content>> userTitleContentMap;
	
	public static ContentDAOSimpleImpl getInstance() throws DataAccessException {
		if (instance == null) {
			instance = new ContentDAOSimpleImpl();
		}
		return instance;
	}
	
	private ContentDAOSimpleImpl() {
		this.contentIDMap = new HashMap<>();
		this.userTitleContentMap = new HashMap<UUID, Map<String, Content>>();
	}
	
	@Override
	public void saveContent(Content content) throws DataAccessException {
		this.contentIDMap.put(content.getContentID(), content);
		Map<String, Content> innerMap = this.userTitleContentMap.get(content.getUserID());
		if (innerMap == null) {
			this.userTitleContentMap.put(content.getUserID(), innerMap = new HashMap<>());
		}
		innerMap.put(content.getTitle(), content);
	}

	@Override
	public Content getContent(UUID contentID) throws DataAccessException {
		return this.contentIDMap.get(contentID);
	}

	@Override
	public Content getUserContent(String title, UUID userID) throws DataAccessException {
		Map<String, Content> innerMap = this.userTitleContentMap.get(userID);
		if (innerMap == null) {
			return null;
		}
		return innerMap.get(title);
	}
	
	@Override
	public List<String> getContentsOfUser(UUID userID) throws DataAccessException {
		Map<String, Content> innerMap = this.userTitleContentMap.get(userID);
		if (innerMap == null) {
			return null;
		}
		// HashMap -> EntrySet -> Stream -> getID -> collect
		return innerMap.entrySet().stream()
				.map(map -> map.getValue().getContentID())
				.map(map -> map.toString())
				.collect(Collectors.toList());
	}
	
	@Override
	public List<String> getContentsByTerms(String terms) throws DataAccessException {
		List<Map<String, Content>> titleMaps = 
				new ArrayList<>(this.userTitleContentMap.values());
		
		List<String> result = new ArrayList<>();
		// for each user
		for (Map<String, Content> map: titleMaps) {
			// Append to result: HashMap -> EntrySet -> Stream -> filter -> getID -> collect
			result.addAll(map.entrySet().stream()
							.filter(m -> m.getKey().contains(terms))
							.map(m -> m.getValue().getContentID())
							.map(m -> m.toString())
							.collect(Collectors.toList()));
		}
		return result;
	}
	
	@Override
	public List<String> getContentsOfUserByTerms(UUID userID, String terms) throws DataAccessException {
		Map<String, Content> innerMap = this.userTitleContentMap.get(userID);
		if (innerMap == null) {
			return null;
		}
		// HashMap -> EntrySet -> Stream -> filter -> getID -> collect
		return innerMap.entrySet().stream()
				.filter(map -> map.getKey().contains(terms))
				.map(map -> map.getValue().getContentID())
				.map(map -> map.toString())
				.collect(Collectors.toList());
	}

	@Override
	public boolean isContentOfUser(UUID contentID, UUID userID) throws DataAccessException {
		Content content = this.contentIDMap.get(contentID);
		boolean result = content == null ? false : content.getUserID().equals(userID);
		return result;
	}
	
	@Override
	public boolean doesContentExist(UUID contentID) throws DataAccessException {
		return this.getContent(contentID) != null;
	}

	@Override
	public void deleteContent(UUID contentID) throws DataAccessException {
		Content content = this.contentIDMap.get(contentID);
		this.contentIDMap.remove(contentID);
		this.userTitleContentMap.get(content.getUserID()).remove(content.getTitle());
	}

	@Override
	public void modifyTitle(UUID contentID, String title) throws DataAccessException {
		Content content = this.contentIDMap.get(contentID);
		content.setTitle(title);
		this.saveContent(content);
	}

	@Override
	public void modifyDescription(UUID contentID, String description) throws DataAccessException {
		Content content = this.contentIDMap.get(contentID);
		content.setDescription(description);
		this.saveContent(content);
	}

}
