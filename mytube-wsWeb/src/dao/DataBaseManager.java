package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DataBaseManager {
	
	private Connection connection;
	private Statement st;
	private static DataBaseManager instance;
	
	private DataBaseManager() throws NamingException, SQLException {
		InitialContext cxt = new InitialContext();
		DataSource ds = (DataSource) cxt.lookup("java:/PostgresXADS");
		if (ds == null) {
			throw new NamingException("Problems connecting to the database");
		}
		this.connection = ds.getConnection();
		this.st = connection.createStatement();
	}
	
	public static DataBaseManager getInstance() throws NamingException, SQLException {
		if (instance == null) {
			instance = new DataBaseManager();
		}
		return instance;
	}
	
	public void update(String sql) throws SQLException {
		this.insertDeleteUpdate(sql);
	}
	
	public void delete(String sql) throws SQLException {
		this.insertDeleteUpdate(sql);
	}
	
	public void insert(String sql) throws SQLException {
		this.insertDeleteUpdate(sql);
	}
	
	private void insertDeleteUpdate(String sql) throws SQLException {
		st.executeUpdate(sql);
	}
	
	public ResultSet query(String sql) throws SQLException {
		return st.executeQuery(sql);
	}
	
	public void close() throws SQLException {
		this.connection.close();
		this.st.close();
	}
}
