package dao;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import data.User;

public class UserDAOSimpleImpl implements UserDAO {
	
	private static UserDAOSimpleImpl instance;
	
	private Map<UUID, User> userMapByID;
	private Map<String, User> userMapByName;
	
	public static UserDAOSimpleImpl getInstance() throws DataAccessException {
		if (instance == null) {
			instance = new UserDAOSimpleImpl();
		}
		return instance;
	}
	
	private UserDAOSimpleImpl() {
		this.userMapByID = new HashMap<>();
		this.userMapByName = new HashMap<>();
	}
	
	@Override
	public void saveUser(User user) throws DataAccessException {
		this.userMapByID.put(user.getUserID(), user);
		this.userMapByName.put(user.getUserName(), user);
	}

	@Override
	public User retreiveUser(String username) throws DataAccessException {
		return this.userMapByName.get(username);
	}

	@Override
	public User retreiveUser(UUID userID) throws DataAccessException {
		return this.userMapByID.get(userID);
	}

}
